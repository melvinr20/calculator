import { connect } from 'react-redux';
import * as actions from '../../actions/CalculatorActions';

import Calculator from '../../components/Calculator';

//Mapping state and dispatch to props for Calculator component and connecting to Store.
const CalculatorContainer = connect(state => state.CalculatorReducer, actions)(Calculator);
export default CalculatorContainer;