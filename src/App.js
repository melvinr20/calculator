import React, { Component } from 'react';
import {Provider} from 'react-redux';
import './App.css';

import Store from './config/store';
import CalculatorContainer from './containers/CalculatorContainer';

class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <CalculatorContainer />
      </Provider>
    );
  }
}

export default App;
