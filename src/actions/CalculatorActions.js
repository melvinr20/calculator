//Basic calculator actions.
// Should be refactored to have separate concerns like 'add', 'subtract', 'divide', etc
export function addElement(char) {
	return {
		type: 'TYPE',
		char
	}
}

export function clear() {
	return {
		type: 'CLEAR'
	}
}

export function equal(value) {
	return {
        type: 'EQUAL',
        value
	}
}