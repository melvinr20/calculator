import React from 'react';
import Button from '../common/Button';  

// Keypad component meant to render all buttons for the calculator
const Keypad = (props) => {
    const { btns, currentValue, clear, addElement, equal} = props;

    return (
        <div>        
            {btns.map((item, key) => {
                // Switch statement to make sure correct functions get passed to correct components
                switch(item) {
                    case "C":
                        return <Button key={key} name={item} clickHandler={clear} value={currentValue}/>
                    case "=":
                        return <Button key={key} name={item} clickHandler={equal} value={currentValue}/>
                    default:
                        return <Button key={key} name={item} clickHandler={addElement} value={currentValue}/>
                }
            }
            )}
        </div>
    );
}

export default Keypad;