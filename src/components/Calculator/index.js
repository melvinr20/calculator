import React from 'react';
import Keypad from '../Keypad';
import Display from '../Display';

const Calculator = (props) => {
    const {
        value,
        btns,
        addElement,
        equal,
        clear
    } = props;

    return (
        <div>
            <Display displayValue={value} currentValue={value}/>
            <Keypad 
                btns={btns} 
                addElement = {addElement}
                equal = {equal}
                clear = {clear}
                currentValue={value}/>
        </div>
    );
}

export default Calculator;