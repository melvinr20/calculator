import React from 'react';

const Display = ({displayValue}) => {
    return (
        <div>
            <p>{displayValue}</p> 
        </div>
    );
}

export default Display;