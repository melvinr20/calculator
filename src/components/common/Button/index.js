import React, {Fragment} from 'react';

const Button = ({name, value, clickHandler}) => {    
    return (
        <Fragment>
            <button onClick={() => clickHandler(value)}>{name}</button>
        </Fragment>
    );
}

export default Button;