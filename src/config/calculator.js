// Configuration file for application, will contain options and possibly configurations
// for external services
const Config = {
    btns: [
        "1", 
        "2", 
        "3",
        "C",
        "4", 
        "5", 
        "6", 
        "=", 
        "7", 
        "8", 
        "9", 
        ".", 
        "+", 
        "0", 
        "-", 
        "*", 
        "/"
    ]
}

export default Config; 