import Config from '../config/calculator';


const initialState = {
    value: 0,
    btns: Config.btns
};


// When there's more time, refactor the calculator logic to have separate concerns like 'add', 'subtract', 'divide', etc
const CalculatorReducer = (state = initialState, action) => {
	switch(action.type) {
		case "CLEAR":
			return {
                ...state,
                value: 0
			};
        case "EQUAL":
            console.log(action);
			return {
                ...state,
                // eval might be harmful, this is purely for demo purposes
                value: eval(action.value) 
			};
		case "ADD_ELEMENT":
			return {
                ...state,
                value: state.value === 0 ? action.char : state.value + action.char
			};
		default:
			return state;
	}
};

export default CalculatorReducer;