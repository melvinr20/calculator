import { combineReducers } from 'redux';
import CalculatorReducer from './CalculatorReducer';

// Combining reducers (if there would be more in the future) so we only have to hook one object to store.
// Reduces clutter in store creation.
const reducers = combineReducers({
	CalculatorReducer
});

export default reducers;